# Libs
Reusable helper configurations are provided here.
Configurations found here should not modify nor trigger any job or action.

## [`rules.yml`](rules.yml)
Provides common rules used in GitLab CI/CD

### Example
```
include:
  project: bmodotdev/gitlab-ci-library
  file: lib/rules.yml

# Trigger on a merge request to your default branch
job1:
  rules:
    - if: !reference [.rules, merge-req-to-default]

# Trigger on commit to your default branch
job2:
  rules:
    - if: !reference [.rules, commit-to-default]

# Trigger on a pipeline schedule
job3:
  rules:
    - if: !reference [.rules, schedule]

# Trigger on downstream multi-project
job4:
  rules:
    - if: !reference [.rules, pipeline]

# Trigger when a parent pipeline calls a downstream pipeline
job5:
  rules:
    - if: !reference [.rules, parent-pipline]

# Trigger on manual run of CI/CD
job6:
  rules:
    - if: !reference [.rules, manual]
```
