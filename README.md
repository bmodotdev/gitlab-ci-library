# GitLab CI Library
A library of commonly used GitLab CI/CD configurations.

# Project Layout
### [`jobs`](jobs)
The `jobs` directory contains “action” jobs or pipelines that can be triggered or included.

### [`lib`](lib)
The `lib` directory contains “noop” configurations that should not initiate any jobs when included.

### [`tests`](tests/)
For most files in the `jobs` directory, a corresponding directory should exist in the `tests`
directory, which proves an example context to exercise the job.
