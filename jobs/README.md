# Jobs
Variables are used within each job to easily allow upstream projects to modify a job as needed.
To modify a job, simply add a `variables:` section to your upstream job.
Read the `variables:` section of each job to see a description and default value.

## [`build-image.yml`](build-image.yml)
Build an image using [kaniko](https://github.com/GoogleContainerTools/kaniko) and push it to the
GitLab Image Registry.

### Example
To build an image when code is merged to your default branch, include the following in your
project’s `.gitlab-ci.yml`:
```
stages:
  - build

Trigger Image Build:
  stage: build
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
  variables:
    IMAGE_AUTHORS: "me@example.com"
    IMAGE_TAGS: "latest,${CI_COMMIT_TAG:-$CI_COMMIT_SHA}"
  trigger:
    strategy: depend
    include:
      - project: bmodotdev/gitlab-ci-library
        file: jobs/build-image.yml
```

## [`scan-image.yml`](scan-image.yml)
Build an [SBOM](https://www.cisa.gov/sbom) using Anchore’s [syft](https://github.com/anchore/syft)
and scan the SBOM using Anchore’s [grype](https://github.com/anchore/grype).

### Example
To scan an image when code is merged to your default branch, include the following in your
project’s `.gitlab-ci.yml`:
```
stages:
  - test

Trigger Image Scan:
  stage: test
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
  trigger:
    strategy: depend
    include:
      - project: bmodotdev/gitlab-ci-library
        file: jobs/scan-image.yml
```
